var _ = require('koa-route');
var json = require("koa-json");
var koa = require('koa');
var app = koa();
var crawler = require("./crawler");
var Promise = require("promise");
var allItems = [];



var luxury = {
    list: function* (next) {


        yield crawler("http://www.luxuryexchange.com/jewelry-and-watches")
            .then(function (items) {
                allItems = items;
            });
        //this.status = 205;
        this.body = yield {
            status: {
                code: 205
            },
            data: allItems
        };
        yield next;


    }

};


app.use(json());
app.use(_.get("/luxury", luxury.list));
app.listen(3003);
console.log('listening on port 3003');
console.log("Please visit 'http://localhost:3003/luxury' to view result");