var cheerio = require("cheerio");
var request = require("request");
var Promise = require('promise');
var co = require("co");
var _ = require("underscore");
var mongoose = require("mongoose");

var db = mongoose.connect("mongodb://smallcar88:mar2016@ds021689.mlab.com:21689/luxify");
var Luxury = require("./models/luxury.model.js");

var getPage = function (url) {
    return new Promise(function (resolve, reject) {
        request({
            url: url,
            method: "GET"
        }, function (err, res, body) {
            if (err) {
                reject(Error("Unable to get the page", err));
            }
            $ = cheerio.load(body);
            var items = [];
            var itemArr = $("#content .prod_hold");

            for (var i = 0; i < itemArr.length; i++) {

                var images = [];
                var price_raw = $(itemArr[i]).find(".price").text();
                var price_arr = [];
                var re = /\$(\d{1,3})?,?\d{1,3}\.[0-9]{1}[0-9]{1}/ig;
                var m;
                while ((m = re.exec(price_raw)) !== null) {
                    if (m.index === re.lastIndex) {
                        re.lastIndex++;
                    }

                    price_arr.push(parseFloat(m[0].replace("$", "").replace(",", "")));

                }

                var imagesHTML = $(itemArr[i]).find(".image a img");
                for (var h = 0; h < imagesHTML.length; h++) {
                    images.push({
                        url: $(imagesHTML[h]).attr("src"),
                        alt_text: $(imagesHTML[h]).attr("alt")
                    });
                }

                var link = $(itemArr[i]).find(".image a").attr("href");
                items.push(new Luxury({
                    _id: $(itemArr[i]).attr("id"),
                    link: link,
                    thumbnails: images,
                    sold: price_arr.length > 0 ? false : true,
                    price: price_arr
                }));

            }
            resolve(items);

        })

    })

}

var saveDB = function (items) {
    return new Promise(function (resolve, reject) {
        for (var i = 0; i < items.length; i++) {
            /*
            items[i].save(function (err, newItem) {
                if (err) console.log(err);
            });
            */
            console.log("Saving item " + items[i]._id);
            Luxury.findOneAndUpdate({
                _id: items[i]._id
            }, items[i], {
                upsert: true
            }, function (err, item) {
                if (err) console.log(err);

            });
        }
        resolve(items);
    });


};

var crawl = function (url) {
    return new Promise(function (resolve, reject) {
        if (_.isUndefined(url)) reject(Error("No url provide."));

        request({
            url: url,
            method: "GET"
        }, function (err, res, body) {
            if (err) reject(Error("Unable to get the number of page", err));
            $ = cheerio.load(body);
            var pageText = $(".pagination .results").text();
            var page;
            var re = /\d{1,3}\sPages/i;
            var m;
            if ((m = re.exec(pageText)) !== null) {
                page = parseInt(m[0].replace(" Pages", ""));
            }
            co(function* () {
                    var allReq = [];
                    for (var i = 1; i <= page; i++) {
                        allReq.push(getPage(url + "?page=" + i));
                    }
                    var allItems = yield allReq;

                    return yield(_.flatten(allItems));

                })
                .then(function (items) {
                    return saveDB(items);
                })
                .then(function (items) {
                    resolve(items);
                })

            .catch(function (err) {
                console.log('Err in executing co:', err);

            });

        });
    })








}
module.exports = crawl;