var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var thumbnailSchema = new Schema({
    url: String,
    alt_text: String
}, {
    _id: false
})



var luxurySchema = new Schema({
    _id: {
        type: String
    },
    link: String,
    thumbnails: [thumbnailSchema],
    sold: Boolean,
    price: [Number]
});

module.exports = mongoose.model("Luxury", luxurySchema);